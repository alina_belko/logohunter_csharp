﻿using Alturos.Yolo;
using logohunter_alturos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace logohunter_charp
{
    class Logohunter
    {
        private YoloWrapper _yolowrapper;

        public Logohunter()
        {
            _yolowrapper = new YoloWrapper("yolo_logohunter.cfg",
                "yolo_logohunter.weights",
                "yolo_logohunter.names");
        }
        public void RunDetection(List<string> imagePaths, List<string> brands = null)
        {
            foreach (string imagePath in imagePaths)
            {
                var logos = _yolowrapper.Detect(imagePath);
                //int i = 0;
                foreach (var logo in logos)
                {
                    Bitmap bmp = ImageUtil.CropImage(new Bitmap(imagePath), logo.X, logo.Y, logo.Width, logo.Height);
                    //save for testing
                    //bmp.Save($"logo{i}.jpg");
                    //i++;
                }
            }
        }

        //public void LoadExtractorModel()
        //{
        //    Inception.RunModel();
        //}

        //public void LoadFeatures()
        //{
        //    using (var reader = new StreamReader(@"features.csv"))
        //    {
        //        List<string> features = new List<string>();
        //        while (!reader.EndOfStream)
        //        {
        //            var line = reader.ReadLine();
        //            features.Add(line);
        //        }
        //    }
        //}
        //public void CalculateCosineSimilarity() { }
    }
}